import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css'
import {BrowserRouter, Route, Switch} from "react-router-dom";

import NaviBar from "./components/NaviBar";
import {Home} from "./components/Home";
import {Market} from "./components/Market";
import {About} from "./components/About";
import {Button} from "react-bootstrap";

function App() {
    return (
        <>
            <BrowserRouter>
                <>
                <NaviBar/>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/market" component={Market}/>
                    <Route exact path="/about" component={About}/>
                </Switch>
                    </>
            </BrowserRouter>
        </>
    );
}

export default App;
